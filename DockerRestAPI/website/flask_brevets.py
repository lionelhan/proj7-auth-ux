"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request, render_template, Response
from flask_restful import Resource, Api
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import csv
from Token import token_required, generate_auth_token
from password import hash_password, verify_password
import logging
from base64 import b64decode

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.caldb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

###
# Register/Token API
###

@app.route("/api/register", methods=["POST"])
def register():
    username = request.form.get("username")
    password = request.form.get("password")
    # error handler -- bad request
    if username == None or password == None:
        return flask.jsonify({"Alert": "Bad request"}), 400
    # exist username
    if db.users.find_one({"username": username}) != None:
        return flask.jsonify({"Alert": "Duplicate Username"}), 400
    # error handler -- END
    hashedPassword = hash_password(password)
    db.users.insert_one({"username": username, "password": hashedPassword})
    jqMessage = {"username": username}
    return flask.jsonify(jqMessage), 201

@app.route("/api/token")
def token():
    authHeader = request.headers.get("Authorization")
    authMode, authString = authHeader.split(" ", 1)
    infoStr = b64decode(authString)
    username, password = infoStr.decode().split(":", 1)
    user = db.users.find_one({"username": username})
    # error handler
    if user == None:
        return flask.jsonify({"Alert": "User not exist"}), 401
    if not verify_password(password, user['password']):
        return flask.jsonify({"Alert": "Invalid password"}), 401
    # error handler -- END
    token = generate_auth_token(expiration=CONFIG.TOKEN_TIME)
    return flask.jsonify({"duration": CONFIG.TOKEN_TIME, "token": token.decode()}), 200
    
    
###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
############### 

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    # receive datas from AJAX
    km = request.args.get('km', 999, type=float)
    miles = request.args.get('miles', 999, type=float)
    brevet_km = request.args.get('brevet_dist_km', 999, type=int)
    begin_time = request.args.get('begin_time')
    begin_date = request.args.get('begin_date')
    # format the arrow time
    datetime = begin_date + ' ' + begin_time

    # set timezone
    start_date_time = arrow.get(datetime).replace(tzinfo='US/Pacific')
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # package and send data back to AJAX
    open_time, message = acp_times.open_time(km, brevet_km, start_date_time.isoformat())
    close_time = acp_times.close_time(km, brevet_km, start_date_time.isoformat())
    result = {"open": open_time, "close": close_time, "message": message}
    return flask.jsonify(result=result)

@app.route("/_result")
def _result():
    _items = db.caldb.find()
    items = [item for item in _items]
    _distance = db.distance.find()
    distance = [distance for distance in _distance]
    print("inside the _display")
    return render_template('result.html', items=items, distance=distance)
@app.route("/_display")
def _display():
    if db.caldb.count() == 0:
        res = "False"
    else:
        res = "True"
    result = {"res": res}
    return flask.jsonify(result=result)
@app.route("/_submit")
def _submit():
    myargs = request.args.get('res')
    print(myargs == "")
    if myargs == "":
        res = "False"
    else:
        res = "True"
        db.caldb.drop()
        db.distance.drop()
        distance = request.args.get('distance', 999, type=int)
        dict_distance = {}
        dict_distance['distance'] = distance
        db.distance.insert_one(dict_distance)
        array = myargs.split(',')
        array_length = len(array)
        pair_num = int(array_length/4)
        counter = 0
        for index in range(pair_num):
            dic = {}
            dic['mile'] = array[counter]
            counter += 1
            dic['km'] = array[counter]
            counter += 1
            dic['opentime'] = array[counter]
            counter += 1
            dic['closetime'] = array[counter]
            counter += 1
            db.caldb.insert_one(dic)
    result = {"res": res}
    return flask.jsonify(result=result)
#############

def get_data():
    _items = db.caldb.find()
    items = [item for item in _items] 
    return items

class listAll(Resource):
    @token_required
    def get(self):
        items = get_data()
        opentime = [item['opentime'] for item in items]
        closetime = [item['closetime'] for item in items]
        return {'Opentime':opentime, 'Closetime':closetime}

class listOpenOnly(Resource):
    @token_required
    def get(self):
        items = get_data()
        opentime = [item['opentime'] for item in items]
        if 'top' not in request.args:   
            return {'Opentime':opentime}
        request_top = request.args.get("top", 0, type=int)
        open_top = []
        counter = request_top
        for item in opentime:
            if counter == 0:
                break
            open_top.append(item)
            counter -= 1
        return {'Opentime':open_top}

class listCloseOnly(Resource):
    @token_required
    def get(self):
        items = get_data()
        closetime = [item['closetime'] for item in items]
        if 'top' not in request.args:   
            return {'Closetime':closetime}
        request_top = request.args.get("top", 0, type=int)
        close_top = []
        counter = request_top
        for item in closetime:
            if counter == 0:
                break
            close_top.append(item)
            counter -= 1
        return {'Closetime':close_top}

class listAll_csv(Resource):
    @token_required
    def get(self):
        items = get_data()
        output = StringIO("")
        filedname = ["opentime", "closetime"]
        # ignore other keys inside dict
        writer = csv.DictWriter(output, filedname, extrasaction='ignore')
        writer.writeheader()
        if 'top' not in request.args:
            for item in items:
                writer.writerow(item)
        request_top = request.args.get("top", 0, type=int)
        counter = request_top
        for item in items:
            if counter == 0:
                break
            else:
                writer.writerow(item)
                counter -= 1
        content = output.getvalue()
        response = Response(content)
        # set output file type
        response.mimetype = "text/csv"
        response.headers['content-Disposition'] = "inline"
        return response

class listOpenOnly_csv(Resource):
    @token_required
    def get(self):
        items = get_data()
        output = StringIO("")
        filedname = ["opentime"]
        # ignore other keys inside dict
        writer = csv.DictWriter(output, filedname, extrasaction='ignore')
        writer.writeheader()
        if 'top' not in request.args:
            for item in items:
                writer.writerow(item)
        request_top = request.args.get("top", 0, type=int)
        counter = request_top
        for item in items:
            if counter == 0:
                break
            else:
                writer.writerow(item)
                counter -= 1
        content = output.getvalue()
        response = Response(content)
        response.mimetype = "text/csv"
        response.headers['content-Disposition'] = "inline"
        return response

class listCloseOnly_csv(Resource):
    @token_required
    def get(self):
        items = get_data()
        output = StringIO("")
        filedname = ["closetime"]
        # ignore other keys inside dict
        writer = csv.DictWriter(output, filedname, extrasaction='ignore')
        writer.writeheader()
        if 'top' not in request.args:
            for item in items:
                writer.writerow(item)
        request_top = request.args.get("top", 0, type=int)
        counter = request_top
        for item in items:
            if counter == 0:
                break
            else:
                writer.writerow(item)
                counter -= 1
        content = output.getvalue()
        response = Response(content)
        response.mimetype = "text/csv"
        response.headers['content-Disposition'] = "inline"
        return response

# Create routes
# Another way, without decorators
api.add_resource(listAll, '/listAll', '/listAll/json', '/')
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json')

api.add_resource(listAll_csv, '/listAll/csv')
api.add_resource(listOpenOnly_csv, '/listOpenOnly/csv')
api.add_resource(listCloseOnly_csv, '/listCloseOnly/csv')
#############
app.secret_key = 'Try one more time'

if __name__ == "__main__":

    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=80, host="0.0.0.0", debug=True)
