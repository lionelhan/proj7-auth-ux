from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, BadSignature, Signature Expired
from functools import wraps
from base64 import b64decode
from flask import request
import flask
import time
import config


# GLOBAL
CONFIG = config.configuration()

def generate_auth_token(expiration=600, key=CONFIG.SECRET_KEY):
   s = Serializer(key, expires_in=expiration)
   return s.dumps({'id': 1})

def verify_auth_token(token, key=CONFIG.SECRET_KEY):
    s = Serializer(key)
    try:
        data = s.loads(token)
    # expired token
    except SignatureExpired:
        return False
    # invalid token    
    except BadSignature:
        return False    
    return True


def token_required(f):
  @wraps(f)
  def decorated_function(*args, **kwargs):
    authHeader = request.headers.get("Authorization")
    authMode, authString = authHeader.split(" ", 1)
    infoStr = b64decode(authString)
    theToken, password = infoStr.decode().split(":", 1)
    if verify_auth_token(theToken):
      return f(*args, **kwargs)
    # error handler
    else:
      return {"Alert":"Invalid token"}, 401
    # error handler -- END
  return decorated_function
